/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.problem;

import java.util.Date;
import java.util.List;

public class ProblemChannel {
	
	private Long textchannelIdLong, userIdLong;
	private String firstMessageIdLong, problemname, problemdescription;
	private List<String> langages;
	private Date startedAt;
	private boolean fixed;
	
	public ProblemChannel(Long textchannelIdLong, String firstMessageIdLong, Long userIdLong, String problemname, List<String> langages, String problemdesciption, Date startedAt, boolean fixed) {
		this.textchannelIdLong = textchannelIdLong;
		this.firstMessageIdLong = firstMessageIdLong;
		this.userIdLong = userIdLong;
		this.problemname = problemname;
		this.problemdescription = problemdesciption;
		this.langages = langages;
		this.startedAt = startedAt;
		this.fixed = fixed;
	}

	public Long getTextchannelIdLong() {
		return textchannelIdLong;
	}
	
	public String getFirstMessageIdLong(){
		return firstMessageIdLong;
	}

	public Long getOwnerIdLong() {
		return userIdLong;
	}

	public String getProblemname() {
		return problemname;
	}

	public String getProblemdescription() {
		return problemdescription;
	}

	public List<String> getLangages() {
		return langages;
	}

	public Date getStartedAt() {
		return startedAt;
	}

	public boolean isFixed() {
		return fixed;
	}

}
