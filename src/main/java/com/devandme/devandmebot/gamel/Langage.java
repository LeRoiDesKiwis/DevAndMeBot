/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.gamel;

import java.util.Arrays;

public enum Langage {
	
	HTML("Html"),
	JAVA("Java");
	
	private String name;
	
	Langage(String name) {
		this.name= name;
	}

	public String getName() {
		return name;
	}

	public static Langage getByName(String language) {
	    return Arrays.stream(values())
                .filter(langage -> langage.name.equalsIgnoreCase(language))
                .findFirst()
                .orElse(null);
    }
}