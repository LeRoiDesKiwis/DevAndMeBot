/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.gamel;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import com.devandme.devandmebot.utils.ResponseList;

import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent;

public class GameLangage {
	
	private static HashMap<User, Integer> bonnesrep = new HashMap<>();
	private static HashMap<Long, String> attenteDeReponse = new HashMap<>();
	
	public void start(User u, Langage l, TextChannel tc){
		
		//Phase de jeu
		for(Entry<String, List<String>> qar : new Questions(l).getQuestions().entrySet()){
			
			//Message "Questions"
			String messageToSend = "  **Question : **\n\n"+qar.getKey()+" ?\n";

			//Message "Reponses"
			ResponseList rl = new ResponseList(qar.getValue(), qar.getValue().get(0));
			messageToSend += String.join(",\n", rl.shuffled().stream().map(String::toString).collect(Collectors.toList()));
			
			String bonneRep = rl.getValidResponse();
			
			Message mes = tc.sendMessage(messageToSend).complete();
			
			mes.addReaction("1⃣").queue();
			mes.addReaction("2⃣").queue();
			mes.addReaction("3⃣").queue();
			mes.addReaction("4⃣").queue();
			attenteDeReponse.put(mes.getIdLong(), bonneRep);
			while(attenteDeReponse.containsKey(mes.getIdLong())){
				//tant que...
			}
		}
		
		//Fin
		tc.sendMessage("Félicitation, vous avez obtenu "+bonnesrep.getOrDefault(u, 0)+" bonnes réponses sur "+new Questions(l).getQuestions().keySet().size()+" questions !").queue();
		
	}
	
	/*public void waitReponse(User u, Message mes, Integer indexBonneReponse){
		//On définit la réponse de l'utilisateur sur la bonne réponse | JUSTE POUR LE DEBUG
		Integer userReply = indexBonneReponse;
		
		if(userReply == indexBonneReponse){
			bonneReponse(u);
			mes.getTextChannel().sendMessage("Bonne réponse ! Félicitation !").queue();
		}else{
			mes.getTextChannel().sendMessage("Malheuresement ce n'est pas la bonne réponse !").queue();
		}
	}*/
	
	private static void bonneReponse(User user){
		int i = 1;
		if(bonnesrep.containsKey(user)){
			i = bonnesrep.get(user);
			bonnesrep.remove(user);
		}
		bonnesrep.put(user, i);
	}

	@SuppressWarnings("unused")
	public static void onReact(MessageReactionAddEvent event) {
		if(attenteDeReponse.containsKey(event.getMessageIdLong())){
			String reactEmot = event.getReactionEmote().getName().toString();
			if((reactEmot.equalsIgnoreCase(convertFromIntToString(1))) || 
					(reactEmot.equalsIgnoreCase(convertFromIntToString(2))) || 
					(reactEmot.equalsIgnoreCase(convertFromIntToString(3))) || 
					(reactEmot.equalsIgnoreCase(convertFromIntToString(4)))){
				String bonneRep = attenteDeReponse.get(event.getMessageIdLong());
				int proptedRep = convertFromStringToInt(reactEmot);
				//Ne marche pas pour le moment
				if(1 == proptedRep){
					bonneReponse(event.getUser());
					event.getTextChannel().sendMessage("Bonne réponse ! Félicitation !").queue();
				}else{
					event.getTextChannel().sendMessage("Malheuresement ce n'est pas la bonne réponse !").queue();
				}
				event.getTextChannel().clearReactionsById(event.getMessageIdLong()).complete();
				attenteDeReponse.remove(event.getMessageIdLong());
			}
		}
	}
	
	private static String convertFromIntToString(int i){
		String r = null;
		switch (i) {
		case 1: r = "1⃣"; break;
		case 2: r = "2⃣"; break;
		case 3: r = "3⃣"; break;
		case 4: r = "4⃣"; break;
		default:
			break;
		}
		return r;
	}
	
	private static int convertFromStringToInt(String str){
		int r = -1;
		switch (str) {
		case "1⃣": r = 1; break;
		case "2⃣": r = 2; break;
		case "3⃣": r = 3; break;
		case "4⃣": r = 4; break;
		default:
			break;
		}
		return r;
	}

}
