/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.core.dataregistery;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;

import com.devandme.devandmebot.core.Configuration;
import com.devandme.devandmebot.core.DevAndMe;

public class DataRegistery {
	
	private static int compteur = 0, commandes = 0;
	
	public DataRegistery(){}
	
	public void saveAll(){
		saveMessagesCount();
		saveCommandesCount();
	}
	
	public void addMessage(){
		compteur++;
	}
	
	private void saveMessagesCount() {
		//Read
		Configuration read = null;
		try (Reader reader = Files.newBufferedReader(Configuration.PATH)) {
		    read = DevAndMe.GSON.fromJson(reader, Configuration.class);
		}catch(IOException e){
			DevAndMe.getLogger().error(e.toString());
		}
		//Calcul
		int previousSessions = read.getMessagesSend();
		int total = (previousSessions + getSessionMessages());
		read.setMessagesSend(total);
		
		//Write
		try (Writer writer = Files.newBufferedWriter(Configuration.PATH)) {
		    DevAndMe.GSON.toJson(read, writer);
		}catch(IOException e){
			DevAndMe.getLogger().error(e.toString());
		}
	}
	
	public int getSessionMessages(){
		return compteur;
	}
	
	public int getAllMessage(){
		//Read
		Configuration read = null;
		try (Reader reader = Files.newBufferedReader(Configuration.PATH)) {
			read = DevAndMe.GSON.fromJson(reader, Configuration.class);
		}catch(IOException e){
			e.printStackTrace();
		}
		int previousSessions = read.getMessagesSend();
		
		return previousSessions + compteur;
	}
	
	public void addCommande(){
		commandes++;
	}
	
	private void saveCommandesCount(){
		//Read
		Configuration read = null;
		try (Reader reader = Files.newBufferedReader(Configuration.PATH)) {
		    read = DevAndMe.GSON.fromJson(reader, Configuration.class);
		}catch(IOException e){
			DevAndMe.getLogger().error(e.toString());
		}
		//Calcul
		int previousSessions = read.getCommandesSend();
		int total = (previousSessions + getSessionCommandes());
		read.setCommandesSend(total);
		
		//Write
		try (Writer writer = Files.newBufferedWriter(Configuration.PATH)) {
		    DevAndMe.GSON.toJson(read, writer);
		}catch(IOException e){
			DevAndMe.getLogger().error(e.toString());
		}
	}
	
	public int getSessionCommandes(){
		return commandes;
	}
	
	public int getAllCommande(){
		//Read
		Configuration read = null;
		try (Reader reader = Files.newBufferedReader(Configuration.PATH)) {
			read = DevAndMe.GSON.fromJson(reader, Configuration.class);
		}catch(IOException e){
			e.printStackTrace();
		}
		int previousSessions = read.getCommandesSend();
		
		return previousSessions + commandes;
	}
}
