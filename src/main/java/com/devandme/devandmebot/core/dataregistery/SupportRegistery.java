/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.core.dataregistery;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.devandme.devandmebot.core.DevAndMe;
import com.devandme.devandmebot.problem.ProblemChannel;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Category;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;

public class SupportRegistery {
	
	private static final Path PATH = Paths.get("support.json");
	private static final Logger LOGGER = LoggerFactory.getLogger(SupportRegistery.class);
	private static SupportRegistery ourInstance = new SupportRegistery();
	
	private final HashMap<Integer, ProblemChannel> problems = new HashMap<>();
	
	private static Category unsolved;
	private static Category solved;
	
	private SupportRegistery(){
		this.load();
	}
	
	public synchronized void load() {
        LOGGER.debug("Chargement des supports...");

        if(Files.exists(PATH)){
            try(BufferedReader reader = Files.newBufferedReader(PATH)){
                this.problems.clear();
                for (SupportData data : DevAndMe.GSON.fromJson(reader, SupportData[].class)) {
                    this.problems.put(data.id, new ProblemChannel(data.textchannelIdLong, data.firstMessageIdLong, data.userIdLong, data.problemname, data.langages, data.problemdescription, data.startedAt, data.fixed));
                }
                LOGGER.debug("{} supports ont été chargés.", this.problems.size());
            }catch (IOException e) {
                LOGGER.error("Impossible de charger la liste des supports !", e);
            }
        }else{
            LOGGER.debug("Aucun supports chargé, le fichier est inexistant.");
        }
    }

    public synchronized void save() {
        LOGGER.debug("Sauvegarde des supports...");
        try (BufferedWriter writer = Files.newBufferedWriter(PATH, StandardCharsets.UTF_8)) {
        	SupportData[] data = this.problems.entrySet().stream().map(SupportData::new).toArray(SupportData[]::new);
            DevAndMe.GSON.toJson(data, writer);
        }catch (IOException e) {
            LOGGER.error("Impossible de charger la liste des supports !", e);
        }

        LOGGER.debug("{} supports ont été sauvegardés.", this.problems.size());
    }
    
    //Get & Set Data
	
    public static void setup(Guild guild){
    	if(guild.getCategoriesByName("Support", true) == null || guild.getCategoriesByName("Support", true).get(0) == null){
			guild.getController().createCategory("Support").queue();
		}
		if(guild.getCategoriesByName("Support - Résolu", true) == null || guild.getCategoriesByName("Support - Résolu", true).get(0) == null){
			guild.getController().createCategory("Support - Résolu").queue();
		}
		Category unsolved = guild.getCategoriesByName("Support", true).get(0);
		Category solved = guild.getCategoriesByName("Support - Résolu", true).get(0);
		SupportRegistery.unsolved = unsolved;
		SupportRegistery.solved = solved;
    }
    
	public ProblemChannel getProblemChannel(int generatedId){
		return problems.get(generatedId);
	}
	
	public int countUserProblems(User user){
		int compteur = 0;
		for(ProblemChannel u : problems.values()){
			if(u.getOwnerIdLong() == null || u.getOwnerIdLong() != user.getIdLong()) continue;
			compteur++;
		}
		return compteur;
	}
	
	public List<ProblemChannel> getAllProblems(User user){
		List<ProblemChannel> problems = new ArrayList<>();
		for(ProblemChannel u : this.problems.values()){
			if(u.getOwnerIdLong() == null || u.getOwnerIdLong() != user.getIdLong()) continue;
			problems.add(u);
		}
		return problems;
	}
	
	public Map<Integer, ProblemChannel> getAllUserProblem(User user){
		Map<Integer, ProblemChannel> userpb = new HashMap<>();
		for(Entry<Integer, ProblemChannel> e : this.problems.entrySet()){
			if(e.getValue().getOwnerIdLong().equals(user.getIdLong())){
				userpb.put(e.getKey(), e.getValue());
			}
		}
		return userpb;
	}
	
	public int getLastUserProblemId(User user){
		int id = -1;
		for(int i : getAllUserProblem(user).keySet()){
			id = i;
		}
		return id;
	}
	
	public String mentionneLangages(Guild guild, List<String> langages){
		String finalMess = null;
		for(String s : langages){
			if(guild.getRolesByName(s, true) != null && guild.getRolesByName(s, true).size() > 0 && guild.getRolesByName(s, true).get(0) != null){
				if(finalMess == null) finalMess = guild.getRolesByName(s, true).get(0).getAsMention();
				else finalMess = finalMess + ", " + guild.getRolesByName(s, true).get(0).getAsMention();
			}else{
				if(s == null) finalMess = s;
				else finalMess = finalMess + ", " + s;
			}
		}
		return finalMess;
	}
	
	public String infosOfProblem(Guild guild, int generatedId){
		if((problems.get(generatedId) != null)){
			ProblemChannel pc = problems.get(generatedId);
			//This is a funfact, if value is "null", return : "AH"
			return 
					"Demande d'aide par " + (pc.getOwnerIdLong() != null ? guild.getMemberById(pc.getOwnerIdLong()).getAsMention() : "AH") + " ("+guild.getTextChannelById(pc.getTextchannelIdLong()).getAsMention()+")\n"
					+ "Problème: " + (pc.getProblemname() != null ? pc.getProblemname() : "Impossible de récupérer le problème") + "\n"
					+ "Langages: " + (pc.getLangages() != null ? mentionneLangages(guild, pc.getLangages()) : "Aucun langage à afficher") + "\n"
					+ "Descriptions rapide: " + (pc.getProblemdescription() != null ? pc.getProblemdescription() : "Aucune information supplémentaire n'a été fourni.") + "\n"
					+ "Statut du problème: " + (pc.isFixed() ? "Résolu" : "Non résolu")
					;
		}
		return null;
	}
	
	public void markAsSolved(Guild guild, int generatedId){
		if((problems.get(generatedId) != null)){
			ProblemChannel pc = problems.get(generatedId);
			if(!pc.isFixed()){
				problems.remove(pc);
				problems.put(generatedId, new ProblemChannel(pc.getTextchannelIdLong(), pc.getFirstMessageIdLong(), pc.getOwnerIdLong(), pc.getProblemname(), pc.getLangages(), pc.getProblemdescription(), pc.getStartedAt(), true));
				EmbedBuilder em = new EmbedBuilder();
				em.setColor(Color.green);
				em.setTitle("Ce problème a été marqué comme Résolu et est maintenant fermé.");
				em.addBlankField(false);
				guild.getTextChannelById(pc.getTextchannelIdLong()).sendMessage(em.build()).queue();
				guild.getTextChannelById(pc.getTextchannelIdLong()).getManager().setParent(solved).queue();
				TextChannel tc = guild.getTextChannelById(pc.getTextchannelIdLong());
				if(tc.getPermissionOverride(guild.getPublicRole()) != null){
					tc.getPermissionOverride(guild.getPublicRole()).delete().queue();
					guild.getManagerUpdatable().update();
				}
				tc.createPermissionOverride(guild.getPublicRole()).setDeny(Permission.MESSAGE_WRITE, Permission.MESSAGE_READ).queue();
				tc.createPermissionOverride(guild.getMemberById(pc.getOwnerIdLong())).setAllow(Permission.MESSAGE_READ).queue();
				guild.getManagerUpdatable().update();
			}
		}
		update(guild, generatedId);
	}
	
	public void markAsUnsolved(Guild guild, int generatedId){
		if(problems.get(generatedId) != null){
			ProblemChannel pc = problems.get(generatedId);
			if(pc.isFixed()){
				problems.remove(pc);
				problems.put(generatedId, new ProblemChannel(pc.getTextchannelIdLong(), pc.getFirstMessageIdLong(), pc.getOwnerIdLong(), pc.getProblemname(), pc.getLangages(), pc.getProblemdescription(), pc.getStartedAt(), false));
				EmbedBuilder em = new EmbedBuilder();
				em.setColor(Color.red);
				em.setTitle("Ce problème a été marqué comme Non-résolu et est à nouveau ouvert.");
				em.addBlankField(false);
				guild.getTextChannelById(pc.getTextchannelIdLong()).sendMessage(em.build()).queue();
				guild.getTextChannelById(pc.getTextchannelIdLong()).getManager().setParent(unsolved).queue();
				guild.getTextChannelById(pc.getTextchannelIdLong()).getPermissionOverride(guild.getPublicRole()).delete().queue();
				guild.getManagerUpdatable().update();
			}
		}
		update(guild, generatedId);
	}
	
	public void setDescription(Guild guild, int generatedId, String description){
		if((problems.get(generatedId) != null)){
			ProblemChannel pc = problems.get(generatedId);
			problems.remove(pc);
			problems.put(generatedId, new ProblemChannel(pc.getTextchannelIdLong(), pc.getFirstMessageIdLong(), pc.getOwnerIdLong(), pc.getProblemname(), pc.getLangages(), description, pc.getStartedAt(), pc.isFixed()));
		}
		update(guild, generatedId);
	}
	
	public void setTitle(Guild guild, int generatedId, String title){
		if((problems.get(generatedId) != null)){
			ProblemChannel pc = problems.get(generatedId);
			problems.remove(pc);
			problems.put(generatedId, new ProblemChannel(pc.getTextchannelIdLong(), pc.getFirstMessageIdLong(), pc.getOwnerIdLong(), title, pc.getLangages(), pc.getProblemname(), pc.getStartedAt(), pc.isFixed()));
		}
		update(guild, generatedId);
	}
	
	private void setFirstMessageIdLong(Guild guild, int generatedId, String firstMessageIdLong){
		if(problems.get(generatedId) != null){
			ProblemChannel pc = problems.get(generatedId);
			problems.remove(pc);
			problems.put(generatedId, new ProblemChannel(pc.getTextchannelIdLong(), firstMessageIdLong, pc.getOwnerIdLong(), pc.getProblemname(), pc.getLangages(), pc.getProblemdescription(), pc.getStartedAt(), pc.isFixed()));
		}
		update(guild, generatedId);
	}
	
	public TextChannel createNewProblem(Guild guild, Long userIdLong, String problemname, List<String> langages){
		int generatedId = problems.size();
		
		TextChannel tc = (TextChannel) guild.getController().createTextChannel("support-"+generatedId).setParent(unsolved).complete();
		
		problems.put(generatedId, new ProblemChannel(tc.getIdLong(), null, userIdLong, problemname, langages, null, Date.from(Instant.now()), false));
		
		guild.getTextChannelById(tc.getIdLong()).getManager().setTopic(infosOfProblem(guild, generatedId)).queue();
		Message first = tc.sendMessage(infosOfProblem(guild, generatedId)).complete();
		
		setFirstMessageIdLong(guild, generatedId, first.getId());
		
		return tc;
	}

	public void update(Guild guild, int generatedId) {
		if(problems.get(generatedId) != null){
			ProblemChannel pc = problems.get(generatedId);
			String idLong = pc.getFirstMessageIdLong();
			guild.getTextChannelById(pc.getTextchannelIdLong()).editMessageById(idLong, infosOfProblem(guild, generatedId)).queue();
			guild.getTextChannelById(pc.getTextchannelIdLong()).getManager().setTopic(infosOfProblem(guild, generatedId)).queue();
			guild.getManagerUpdatable().update();
		}
	}
	
	public Long getProblemOwnerIdLong(int generatedId){
		if(problems.get(generatedId) != null){
			return problems.get(generatedId).getOwnerIdLong();
		}
		return -1L;
	}
	
	public List<ProblemChannel> findProblemsByName(String[] mots){
		List<ProblemChannel> proc = new ArrayList<>();
		for(String s : mots){
			for(ProblemChannel pc : problems.values()){
				if((pc.getProblemname() != null && pc.getProblemname().contains(s)) || (pc.getProblemdescription() != null && pc.getProblemdescription().contains(s)) || pc.getLangages().contains(s)){
					proc.add(pc);
					break;
				}
			}
		}
		return proc;
	}
    
    //End of Data

    public static SupportRegistery getInstance() {
        return ourInstance;
    }

    private final class SupportData {

    	public int id;
    	public Long textchannelIdLong, userIdLong;
    	public String firstMessageIdLong, problemname, problemdescription;
    	public List<String> langages;
    	public Date startedAt;
    	public boolean fixed;
    	
    	public SupportData(Map.Entry<Integer, ProblemChannel> entry) {
    		this.id = entry.getKey();
    		this.textchannelIdLong = entry.getValue().getTextchannelIdLong();
    		this.firstMessageIdLong = entry.getValue().getFirstMessageIdLong();
    		this.userIdLong = entry.getValue().getOwnerIdLong();
    		this.problemname = entry.getValue().getProblemname();
    		this.problemdescription = entry.getValue().getProblemdescription();
    		this.langages = entry.getValue().getLangages();
    		this.startedAt = entry.getValue().getStartedAt();
    		this.fixed = entry.getValue().isFixed();
        }
    }
	
}
