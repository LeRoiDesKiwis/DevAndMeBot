/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.core;

import com.devandme.devandmebot.commands.CommandMap;
import com.devandme.devandmebot.core.dataregistery.DataRegistery;
import com.devandme.devandmebot.core.dataregistery.GuildDataRegistery;
import com.devandme.devandmebot.core.dataregistery.PlayerDataRegistery;
import com.devandme.devandmebot.core.dataregistery.SupportRegistery;
import com.devandme.devandmebot.core.runnable.GameRunnableManager;
import com.devandme.devandmebot.core.runnable.MessageBrodcaster;
import com.devandme.devandmebot.listeners.BotListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class DevAndMe {

    private static final Logger LOGGER = LoggerFactory.getLogger(DevAndMe.class);

    public static Gson GSON = new GsonBuilder().serializeNulls().setPrettyPrinting().create();

    private final ScheduledExecutorService executorService;

    private final JDA jda;

    private final Configuration configuration;

    private final CommandMap commandMap;

    private volatile boolean running = true;

    public DevAndMe() throws Exception {
        LOGGER.info("Démarrage de DevAndMeBot...");

        this.configuration = Configuration.load();
        
        this.executorService = Executors.newScheduledThreadPool(1, new DaemonThreadFactory());

        this.commandMap = new CommandMap(this, configuration);

        jda = new JDABuilder(AccountType.BOT).setToken(this.configuration.getDiscordToken()).buildAsync();
        jda.addEventListener(new BotListener(commandMap));
        
        GameRunnableManager.start(this.executorService, this.configuration, this.jda);
        
        this.executorService.scheduleAtFixedRate(
        		new MessageBrodcaster(this.configuration.getMessagesBroadcast(), this.jda), 
        		0L, 
        		6, 
        		TimeUnit.HOURS);
        
        LOGGER.info("Bot démarré!");
    }

    public JDA getJda() {
        return jda;
    }

    public void loop() {
        try (Scanner scanner = new Scanner(System.in)) {
            while (running) {
                commandMap.commandConsole(scanner.nextLine());
            }
        }
    }

    public boolean isRunning() {
        return running;
    }

    public void stop() {
        this.running = false;

        this.jda.shutdown();
        new DataRegistery().saveAll();
        PlayerDataRegistery.getInstance().save();
        SupportRegistery.getInstance().save();
        GuildDataRegistery.getInstance().save();
    }

    @Override
    protected void finalize() throws Throwable {
        this.executorService.shutdown();
    }
    
    public static Logger getLogger(){
    	return LOGGER;
    }
    
}