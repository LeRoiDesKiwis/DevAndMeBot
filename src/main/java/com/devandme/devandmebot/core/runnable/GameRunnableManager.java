/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.core.runnable;

import java.util.Objects;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import com.devandme.devandmebot.core.Configuration;

import net.dv8tion.jda.core.JDA;

public abstract class GameRunnableManager {
	
	private static ScheduledFuture<?> sf; 
	
	protected static ScheduledExecutorService executorService;
	protected static Configuration configuration;
	protected static JDA jda;
	
	public static void start(ScheduledExecutorService executorService, Configuration configuration, JDA jda){
		Objects.requireNonNull(executorService);
		Objects.requireNonNull(configuration);
		Objects.requireNonNull(jda);

		sf = executorService.scheduleAtFixedRate(
                new GameSwitcher(configuration.getGames(), jda),
                0,
                configuration.getGameRotationDelay(),
                configuration.getGameRotationDelayUnit()
        );
	}
	
	public static void start(){
		if(executorService != null && configuration != null && jda != null){ 
			sf = executorService.scheduleAtFixedRate(
	                new GameSwitcher(configuration.getGames(), jda),
	                0,
	                configuration.getGameRotationDelay(),
	                configuration.getGameRotationDelayUnit()
	        );
		}
	}
	
	public static void stop(){
		sf.cancel(true);
	}
}