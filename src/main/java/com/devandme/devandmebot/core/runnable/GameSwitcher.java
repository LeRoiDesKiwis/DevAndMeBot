/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.core.runnable;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Game;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class GameSwitcher implements Runnable {

    private final List<Game> games;

    private final JDA jda;

    private int index;

    public GameSwitcher(Collection<String> games, JDA jda) {
        this.jda = jda;
        this.index = 0;
        this.games = games.stream().map(Game::playing).collect(Collectors.toList());
    }

    @Override
    public void run() {

        if (this.games.isEmpty()) {
            return;
        }

        if (this.index >= this.games.size()) {
            this.index = 0;
        }

        this.jda.getPresence().setGame(this.games.get(this.index));

        this.index++;
    }
}