/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.utils;

public class MessageManager {
	
	public static String getMessage(MessagesList messagePath){
		return (messagePath.getMessage() != null ? messagePath.getMessage() : messagePath.toString());
	}
	
	public enum MessagesList {
		CMD_HELP_LIST("Liste des commandes :"),
		;
		
		private String message = null;
		
		MessagesList(String message){
			this.message = message;
		}
		
		public String getMessage(){
			return message;
		}
	}

}
