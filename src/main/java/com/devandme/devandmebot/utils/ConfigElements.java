/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.utils;

import java.util.HashMap;

public class ConfigElements {
	
	private Long mutedRoleName, mainChannelName, staffChannelName, spamChannelName;
	private HashMap<String, Integer> rankspower;
	
	public ConfigElements(Long mutedRoleName, Long mainChannelName, Long staffChannelName, Long spamChannelName, HashMap<String, Integer> rankspower){
		this.mutedRoleName = mutedRoleName;
		this.mainChannelName = mainChannelName;
		this.staffChannelName = staffChannelName;
		this.spamChannelName = spamChannelName;
		this.rankspower = rankspower;
	}

	public Long getMutedRoleName() {
		return mutedRoleName;
	}
	
	public Long getMainChannelName() {
		return mainChannelName;
	}

	public Long getStaffChannelName() {
		return staffChannelName;
	}

	public Long getSpamChannelName() {
		return spamChannelName;
	}
	
	public HashMap<String, Integer> getRanksPower() {
		return rankspower;
	}

}
