/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.utils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Une liste de réponses possibles.
 */
public class ResponseList {

    private final Set<String> responses = new HashSet<>();

    private final String validResponse;

    public ResponseList(Collection<String> responses, String validResponse) {
        Objects.requireNonNull(responses);
        Objects.requireNonNull(validResponse);

        this.validResponse = validResponse;

        if (!responses.contains(validResponse)) {
            responses.add(validResponse);
        }

        this.responses.addAll(responses);
    }

    public Set<String> getResponses() {
        return responses;
    }

    public String getValidResponse() {
        return validResponse;
    }

    public List<String> shuffled() {
        return this.responses.stream().sorted(new RandomComparator<>()).collect(Collectors.toList());
    }

    public boolean isResponseValid(String response) {
        return this.validResponse.equalsIgnoreCase(response);
    }
}