/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.utils;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAccessor;
import java.util.Locale;
import java.util.Objects;

/**
 * Utility class that helps with dates.
 */
public final class DateUtils {

    /**
     * French date/time formatter (long version).
     * <p>
     * Example: Lundi 4 Janvier 2016 à 23:51.
     * </p>
     */
    public static final DateTimeFormatter DATE_TIME_FORMATTER_FRENCH_LONG
            = DateTimeFormatter.ofPattern("EEEE d MMMM uuuu 'à' HH:mm", Locale.FRENCH);

    /**
     * French date/time formatter (short version).
     * <p>
     * Example: 23:51 - 04.01.2016.
     * </p>
     */
    public static final DateTimeFormatter DATE_TIME_FORMATTER_FRENCH_SHORT
            = DateTimeFormatter.ofPattern("HH:mm - dd.MM.uuuu", Locale.FRENCH);

    /**
     * French date formatter (long version).
     * <p>
     * Example: Lundi 4 Janvier 2016.
     * </p>
     */
    public static final DateTimeFormatter DATE_FORMATTER_FRENCH_LONG
            = DateTimeFormatter.ofPattern("EEEE d MMMM uuuu", Locale.FRENCH);

    /**
     * French date formatter (short version).
     * <p>
     * Example: 04.01.2016.
     * </p>
     */
    public static final DateTimeFormatter DATE_FORMATTER_FRENCH_SHORT
            = DateTimeFormatter.ofPattern("dd.MM.uuuu", Locale.FRENCH);

    private DateUtils() { }

    /**
     * Formats the difference between two dates in french format.
     *
     * @param start The start date.
     * @param end   The end date.
     * @return The difference, formatted.
     */
    public static String formatDifference(Temporal start, Temporal end) {
        Objects.requireNonNull(start, "The parameter start cannot be null!");
        Objects.requireNonNull(end, "The parameter end cannot be null!");

        //If the elapsed time is less than a second we return a custom label.
        if (start.isSupported(ChronoUnit.MILLIS) && end.isSupported(ChronoUnit.MILLIS)
                && start.until(end, ChronoUnit.MILLIS) < 1000) {
            return "moins d'une seconde";
        }

        Period period = Period.between(LocalDate.from(start), LocalDate.from(end)).normalized();

        //Difference in years.
        long years = period.getYears();
        boolean showYears = years > 0;

        //Difference in months.
        long months = period.getMonths();
        boolean showMonths = months > 0 && years <= 1;

        //Difference in days.
        long days = period.getDays();
        boolean showDays = days > 0 && !showYears;

        StringBuilder result = new StringBuilder();

        if (showYears) {
            result.append(years).append(years == 1 ? " an" : " ans");
        }

        if (showMonths) {
            if (showYears) {
                result.append(", ");
            }

            result.append(months).append(" mois");
        }

        if (showDays) {
            if (showMonths) {
                result.append(", ");
            }

            result.append(days).append(days == 1 ? " jour" : " jours");
        }

        if (start.isSupported(ChronoField.SECOND_OF_MINUTE) && end.isSupported(ChronoField.SECOND_OF_MINUTE)) {

            //We separate the date from the time so the total number of seconds will be used for the day only.
            long seconds = Duration.between(LocalTime.from(start), LocalTime.from(end)).getSeconds();

            //Difference in hours.
            long hours = Math.floorDiv(seconds, 3600);
            boolean showHours = hours > 0 && days <= 2 && !showYears && !showMonths;

            if (hours > 0) {
                seconds %= 3600;
            }

            //Difference in minutes.
            long minutes = Math.floorDiv(seconds, 60);
            boolean showMinutes = minutes > 0 && !showYears && !showMonths && !showDays;

            if (minutes > 0) {
                seconds %= 60;
            }

            boolean showSeconds = seconds > 0 && !showYears && !showMonths && !showDays && !showHours;

            //Si les heures peuvent être affichées, on les ajoute.
            if (showHours) {
                if (showDays) {
                    result.append(", ");
                }

                result.append(hours).append(hours == 1 ? " heure" : " heures");
            }

            //Si les minutes peuvent être affichées, on les ajoute.
            if (showMinutes) {
                if (showHours) {
                    result.append(", ");
                }

                result.append(minutes).append(minutes == 1 ? " minute" : " minutes");
            }

            //Enfin, si les secondes peuvent être affichées, on les ajoute.
            if (showSeconds) {
                if (showMinutes) {
                    result.append(", ");
                }

                result.append(seconds).append(seconds == 1 ? " seconde" : " secondes");
            }
        }

        return result.toString();
    }

    /**
     * Formats the provided date with the {@link #DATE_FORMATTER_FRENCH_SHORT}.
     *
     * @param temporalAccessor The date to format.
     * @return The formatted date.
     */
    public static String formatShort(TemporalAccessor temporalAccessor) {
        return temporalAccessor == null ? null : DATE_FORMATTER_FRENCH_SHORT.format(temporalAccessor);
    }

    /**
     * Formats the provided date with the {@link #DATE_TIME_FORMATTER_FRENCH_LONG}.
     *
     * @param temporal The date to format.
     * @return The formatted date.
     */
    public static String formatLong(TemporalAccessor temporal) {
        return temporal == null ? null : DATE_FORMATTER_FRENCH_LONG.format(temporal);
    }
}