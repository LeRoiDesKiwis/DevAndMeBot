/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.commands.defaults.user;

import java.util.List;

import com.devandme.devandmebot.commands.Command;
import com.devandme.devandmebot.core.dataregistery.PlayerDataRegistery;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

public class MedalsCommand {
	
	@Command(name="medal", description="Gestion des médailles")
	private void power(User user, MessageChannel channel, Message message, Guild guild, String[] args){
		PlayerDataRegistery pdr = PlayerDataRegistery.getInstance();
		List<String> medals = pdr.getMedals(user.getIdLong());
		channel.sendMessage("Vous avez "+medals.size()+" :medal:\n"+medals(medals)).queue();
	}
	
	private String medals(List<String> string){
		String str = null;
		for(String s : string){
			if(str == null){
				str = "  :medal: " + s;
			}else{
				str = str + "\n  :medal: " + s;
			}
		}
		return str;
	}

}
