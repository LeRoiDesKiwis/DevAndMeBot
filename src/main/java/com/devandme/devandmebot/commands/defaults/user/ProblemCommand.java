/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.commands.defaults.user;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.devandme.devandmebot.commands.Command;
import com.devandme.devandmebot.commands.CommandMap;
import com.devandme.devandmebot.commands.Command.ExecutorType;
import com.devandme.devandmebot.core.dataregistery.SupportRegistery;
import com.devandme.devandmebot.problem.ProblemChannel;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;

public class ProblemCommand {
	
	/*
	 * Long: User IdLong
	 * Integer: Progression dans la création du salon
	 * String: Resultat pour cette progression
	 */
	public static Map<Long, Map<Integer, String>> pbc = new HashMap<>();
	
	private CommandMap commandmap;
	
	public ProblemCommand(CommandMap commandMap) {
		this.commandmap = commandMap;
	}

	public static void validMessage(Guild guild, User user, String message, TextChannel channel) {
		int state = pbc.get(user.getIdLong()).size();
		Map<Integer, String> loadedHashMap = pbc.get(user.getIdLong());
		if((message != null) && (message.equalsIgnoreCase("-c"))){
			channel.sendMessage("Problème » Création annulé !").queue();
			pbc.remove(user.getIdLong());
			return;
		}
		switch (state) {
		case 0: //il n'a rien mis dans la map encore, il souhaite mettre le titre
			loadedHashMap.put(state, message);
			pbc.remove(user.getIdLong());
			pbc.put(user.getIdLong(), loadedHashMap);
			channel.sendMessage("Problème » Le titre de votre problème a été défini sur: **"+message+"**.\nProblème » Entrez apprésent une **courte description** de votre problème.").queue();
			break;
		case 1:
			loadedHashMap.put(state, message);
			pbc.remove(user.getIdLong());
			pbc.put(user.getIdLong(), loadedHashMap);
			channel.sendMessage("Problème » La description de votre problème a été défini sur: **"+message+"**.\nProblème » Entrez maintenant les langages touchés par votre problème (séparés par un espace). _(Note: Si les langages existent sur le serveur, ils  apparaîtront en couleur)_").queue();
			break;
		case 2:
			List<String> l = new ArrayList<>();
			String langages = null;
			String[] splited = message.split(" ");
			for(int i = 0; i < splited.length; i++){
				if(langages == null) langages = splited[i].toString();
				else langages = langages + ", " + splited[i].toString();
				l.add(splited[i].toString());
			}
			if(langages == null){
				channel.sendMessage("Problème » Impossible de récupérer une liste valide de langages : essayer à nouveau ou entrez **-c** pour annulé.").queue();
				return;
			}
			loadedHashMap.put(state, langages);
			pbc.remove(user.getIdLong());
			pbc.put(user.getIdLong(), loadedHashMap);
			
			channel.sendMessage("Problème » Les langages concernés par le problème sont: **"+langages+"**\nCréation de votre salon en cours...").queue();
			
			String probName = pbc.get(user.getIdLong()).get(0);
			String descri = pbc.get(user.getIdLong()).get(1);
			
			pbc.remove(user.getIdLong());
			
			TextChannel b = SupportRegistery.getInstance().createNewProblem(guild, user.getIdLong(), probName, l);
		    
			int id = SupportRegistery.getInstance().getLastUserProblemId(user);
			if(id != -1){
				SupportRegistery.getInstance().setDescription(guild, id, descri);
			}
			
			//On vérifie que le salon a bien été créé
	        if(b != null){
	        	channel.sendMessage("Problème » Votre salon "+b.getAsMention()+" a été créé avec succès").queue();
	        }
	        break;
		default:
			break;
		}
	}

	@Command(name="problem", type=ExecutorType.ALL_USER, description="Ouvrir une nouvelle demande d'aide")
	private void problem(Guild guild, User user, MessageChannel channel, Message message, String[] args){
		SupportRegistery.setup(guild);
		
		if(args.length >= 1){
			
			if(args[0].equalsIgnoreCase("create")){
				if(pbc.get(user.getIdLong()) == null){
					//Première fois qu'il fait la cmd
					channel.sendMessage("Problème » Vous avez démarrer la création d'un nouveau problème. Entrer un titre.\n\n_Vous pouvez annulé la création du problème à tout moment en entrant _**-c**.").queue();
					Map<Integer, String> empty = new HashMap<>();
					pbc.put(user.getIdLong(), empty);
					return;
				}
			}else if((args[0].equalsIgnoreCase("info")) && (args.length == 2)){
				int id = Integer.parseInt(args[1].replaceFirst("support", "").replaceFirst("-", ""));
				String pm = SupportRegistery.getInstance().infosOfProblem(guild, id);
				if(pm == null){
					channel.sendMessage("_Impossible de récuperer les infos de "+id+"!_").queue();
					return;
				}
				channel.sendMessage(pm).queue();
				return;
			}else if(args[0].equalsIgnoreCase("solved") && (args.length >= 1)){
				if(args.length == 1){
					//Get is problem id
					if(SupportRegistery.getInstance().getAllProblems(user).size() != 1){
						channel.sendMessage("_Vous devez spécifier l'ID du problème en question._").queue();
						return;
					}
				}
				int id = 
						(args.length == 1 ? 
								SupportRegistery.getInstance().getLastUserProblemId(user) : 
									Integer.parseInt(args[1].replaceFirst("support", "").replaceFirst("-", "")));
				String pm = SupportRegistery.getInstance().infosOfProblem(guild, id);
				if(pm == null){
					channel.sendMessage("_Impossible de récuperer les infos de "+id+"!_").queue();
					return;
				}
				if(!canInteractWithProblem(guild, user, id)){
					channel.sendMessage("_Vous n'êtes pas le créateur de ce problème !_").queue();
					return;
				}
				SupportRegistery.getInstance().markAsSolved(guild, id);
				channel.sendMessage("Votre problème est maintenant marqué comme 'Résolu'.").queue();
				return;
			}else if(args[0].equalsIgnoreCase("unsolved") && (args.length == 2)){
				int id = Integer.parseInt(args[1].replaceFirst("support", "").replaceFirst("-", ""));
				String pm = SupportRegistery.getInstance().infosOfProblem(guild, id);
				if(pm == null){
					channel.sendMessage("_Impossible de récuperer les infos de "+id+"!_").queue();
					return;
				}
				if(!canInteractWithProblem(guild, user, id)){
					channel.sendMessage("_Vous n'êtes pas le créateur de ce problème !_").queue();
					return;
				}
				SupportRegistery.getInstance().markAsUnsolved(guild, id);
				channel.sendMessage("Votre problème n'est maintenant plus marqué comme 'Résolu'.").queue();
				return;
			}else if(args[0].equalsIgnoreCase("description") && (args.length >= 2)){
				int id = Integer.parseInt(args[1].replaceFirst("support", "").replaceFirst("-", ""));
				if(SupportRegistery.getInstance().infosOfProblem(guild, id) == null){
					channel.sendMessage("_Impossible de récuperer les infos de "+id+"!_").queue();
					return;
				}
				if(!canInteractWithProblem(guild, user, id)){
					channel.sendMessage("_Vous n'êtes pas le créateur de ce problème !_").queue();
					return;
				}
				StringBuilder sb = new StringBuilder();
				for(int j = 2; j < args.length; j++){
					sb.append(args[j]).append(" ");
				}
				String descri = sb.toString().trim();

		        SupportRegistery.getInstance().setDescription(guild, id, descri);
				channel.sendMessage("Votre problème a une nouvelle description: "+descri+".").queue();
				return;
			}else if(args[0].equalsIgnoreCase("title") && (args.length >= 2)){
				int id = Integer.parseInt(args[1].replaceFirst("support", "").replaceFirst("-", ""));
				if(SupportRegistery.getInstance().infosOfProblem(guild, id) == null){
					channel.sendMessage("_Impossible de récuperer les infos de "+id+"!_").queue();
					return;
				}
				if(!canInteractWithProblem(guild, user, id)){
					channel.sendMessage("_Vous n'êtes pas le créateur de ce problème !_").queue();
					return;
				}
				StringBuilder sb = new StringBuilder();
				for(int j = 2; j < args.length; j++){
					sb.append(args[j]).append(" ");
				}
				String title = sb.toString().trim();

		        SupportRegistery.getInstance().setTitle(guild, id, title);
				channel.sendMessage("Votre problème a un nouveau titre: "+title+".").queue();
				return;
			}else if(args[0].equalsIgnoreCase("view") && (args.length == 2)){
				int id = Integer.parseInt(args[1].replaceFirst("support", "").replaceFirst("-", ""));
				String pm = SupportRegistery.getInstance().infosOfProblem(guild, id);
				if(pm == null){
					channel.sendMessage("_Impossible de récuperer les infos de "+id+"!_").queue();
					return;
				}
				TextChannel toView = guild.getTextChannelById(SupportRegistery.getInstance().getProblemChannel(id).getTextchannelIdLong());
				toView.createPermissionOverride(message.getMember()).setAllow(Permission.MESSAGE_READ).complete();
				toView.getPermissionOverride(message.getMember()).delete().queueAfter(20, TimeUnit.MINUTES);
				channel.sendMessage("Vous avez maintenant accès au salon d'aide "+toView.getAsMention()+" pour 20 minutes !").queue();
				return;
			}else if(args[0].equalsIgnoreCase("find") && (args.length >= 2)){
				StringBuilder sb = new StringBuilder();
				for(int j = 1; j < args.length; j++){
					sb.append(args[j]).append(" ");
				}
				String[] listOfWord = sb.toString().trim().split(" ");
				List<ProblemChannel> pl = SupportRegistery.getInstance().findProblemsByName(listOfWord);
				channel.sendMessage("Il y a **"+pl.size()+"** problème"+(pl.size() > 1 ? "s" : "")+" qui correspond"+(pl.size() > 1 ? "ent" : "")+" à votre recherche"+(pl.size() >= 1 ? ":" : "." )).queue();
				for(ProblemChannel pc : pl){
					channel.sendMessage(guild.getTextChannelById(pc.getTextchannelIdLong()).getAsMention()+" - "+pc.getProblemname()+" - "+pc.getProblemdescription()+" - "+(pc.isFixed() ? "Résolu" : "Non-résolu")).queue();
				}
				return;
			}
			
		}
		
		//Displey help
		EmbedBuilder em = new EmbedBuilder();
		em.setTitle("Aide: !problem");
		em.setDescription("");
		em.setColor(Color.lightGray);
		em.addField("!problem create", "Ouvrir une nouvelle demande d'aide", true);
		em.addField("!problem info [id]", "Affiche les informations relatives à une demande d'aide", true);
		em.addField("!problem (un)solved (id)", "Change le status d'une demande d'aide en Résolu / Non-Résolu.", true);
		em.addField("!problem title [id] [nouveau titre...]", "Change le titre d'une demande d'aide", true);
		em.addField("!problem description [id] [nouvelle description...]", "Change la description d'une demande d'aide", true);
		em.addField("!problem view [id]", "Vous donne accès au salon relatif à la demande d'aide pour 20 minutes (même si le problème est noté comme résolu)", true);
		em.addField("!problem find [mots à filtrer]", "Chercher des problèmes similaires via des mots clés", true);
		channel.sendMessage(em.build()).queue();
	}
	
	public boolean canInteractWithProblem(Guild guild, User user, int problemid){
		if((!SupportRegistery.getInstance().getProblemOwnerIdLong(problemid).equals(user.getIdLong())) && (commandmap.getTotalUserPower(guild, user) < 70)){
			return false;
		}
		return true;
	}

}
