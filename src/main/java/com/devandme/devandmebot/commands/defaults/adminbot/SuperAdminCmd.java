/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.commands.defaults.adminbot;

import com.devandme.devandmebot.commands.Command;
import com.devandme.devandmebot.commands.Command.ExecutorType;
import com.devandme.devandmebot.core.runnable.GameRunnableManager;

import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

public class SuperAdminCmd {
	
	@Command(name="stream", type=ExecutorType.S_USER)
	private void stream(Guild guild, User user, MessageChannel channel, String[] args){
		if((args.length >= 1) && (Game.isValidStreamingUrl(args[0].toString()) || (args[0].equalsIgnoreCase("off")))){
			String link = args[0].toString();
			if(link.equalsIgnoreCase("off")){
				GameRunnableManager.start();
				channel.sendMessage("Le stream a pris fin !").queue();
				return;
			}
			GameRunnableManager.stop();
			channel.sendMessage("Le stream commence !").queue();
			guild.getJDA().getPresence().setGame(Game.streaming("Regarde mon développement en direct", link));
			guild.getDefaultChannel().sendMessage("[Live] Un live vient de commencer ! Regarde mon développement en direct sur "+link+" !").queue();
			return;
		}
		
		channel.sendMessage("Vous devez préciser un lien valide ou écrire **off**.").queue();
	}

	@Command(name="broadcast", type=ExecutorType.S_USER)
	private void broadcast(Guild guild, MessageChannel channel, User user, Message message, String[] args){
		if(message.getMentionedChannels().size() <= 0){
			channel.sendMessage("Vous devez mentionner un ou plusieurs salons !").queue();
			return;
		}
		
		StringBuilder sb = new StringBuilder();
		for(int j = 1; j < args.length; j++){
			sb.append(args[j]).append(" ");
		}
		String msg = sb.toString().trim();
		
		message.getMentionedChannels().get(0).sendMessage(msg).queue();
		channel.sendMessage("Message envoyé avec succès dans "+message.getMentionedChannels().get(0).getAsMention()+" !").queue();
		
	}

}
