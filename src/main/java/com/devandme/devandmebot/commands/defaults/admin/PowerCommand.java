/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.commands.defaults.admin;

import com.devandme.devandmebot.commands.Command;
import com.devandme.devandmebot.commands.Command.ExecutorType;
import com.devandme.devandmebot.commands.CommandMap;

import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

public class PowerCommand {
	
	/*
	 * TODO
	 * Il faudrait que les power soient par GUILD!!!
	 */
	
	private final CommandMap commandMap;
	
	public PowerCommand(CommandMap commandMap) {
		this.commandMap = commandMap;
	}
	
	@Command(name="power", type=ExecutorType.ALL_USER, description="Gerer le power des utilisateurs", power=100)
	private void power(User user, MessageChannel channel, Message message, String[] args){
		if(args.length == 0 && message.getMentionedUsers().size() == 0){
			channel.sendMessage("power <power> <@user>").queue();
			return;
		}
		
		int power = 0;
		try{
			power = Integer.parseInt(args[0]);
		}catch(NumberFormatException nfe){
			channel.sendMessage("Le power doit être un nombre valide.").queue();
			return;
		}
		
		if(power > 100) power = 100;
		
		User target = message.getMentionedUsers().get(0);
		commandMap.setPersonalUserPower(target, power);
		channel.sendMessage("Le power de "+target.getAsMention()+" est maintenant de "+power).queue();
	}

}
