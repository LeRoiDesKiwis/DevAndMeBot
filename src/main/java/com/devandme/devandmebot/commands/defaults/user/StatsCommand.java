/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.commands.defaults.user;

import java.awt.Color;

import com.devandme.devandmebot.commands.Command;
import com.devandme.devandmebot.core.dataregistery.DataRegistery;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

import static com.devandme.devandmebot.utils.Functions.*;

public class StatsCommand {
	
	@Command(name="stats", description="Affiche divers statistiques relatifs au bot")
	private void power(User user, MessageChannel channel, Message message, String[] args){
		int users = channel.getJDA().getUsers().size();
		int messagesTotaux = new DataRegistery().getAllMessage();
		int commandesTotaux = new DataRegistery().getAllCommande();
		
		EmbedBuilder embedBuilder = new EmbedBuilder();
		embedBuilder.setTitle("Statistiques :");
		embedBuilder.setColor(Color.yellow);
		embedBuilder.addField("Nombre d'utilisateurs :", formatInteger(users), false);
		embedBuilder.addField("Messages totaux :", formatInteger(messagesTotaux), false);
		embedBuilder.addField("Messages cette session :", formatInteger(new DataRegistery().getSessionMessages()), false);
		double calcul = ((double)messagesTotaux / (double)users);
		embedBuilder.addField("Messages moyen par utilisateur :", String.format("%.1f", calcul), false);
		embedBuilder.addField("Commandes totaux :", formatInteger(commandesTotaux), false);
		embedBuilder.addField("Commandes cette session :", formatInteger(new DataRegistery().getSessionCommandes()), false);
		double calcul2 = ((double)commandesTotaux / (double)users);
		embedBuilder.addField("Nombre de commandes moyenne par utilisateur :", String.format("%.1f", calcul2), false);

		channel.sendMessage(embedBuilder.build()).queue();
	}
	
}
