/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.commands.defaults.user;

import com.devandme.devandmebot.commands.Command;
import com.devandme.devandmebot.commands.CommandMap;
import com.devandme.devandmebot.core.dataregistery.PlayerDataRegistery;
import com.devandme.devandmebot.utils.Functions;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.*;

import java.awt.*;

import static com.devandme.devandmebot.utils.DateUtils.formatShort;

public class MeCommand {

    private final CommandMap commandMap;

    public MeCommand(CommandMap commandMap) {
        this.commandMap = commandMap;
    }

    @Command(name = "me", description = "Afficher des informations relatives à votre compte")
    private void me(User user, MessageChannel channel, Message message, Guild guild, String[] args) {
        if ((message.getMember().hasPermission(Permission.MANAGE_SERVER)) && (message.getMentionedUsers() != null) && (message.getMentionedUsers().size() == 1) && (!message.getMentionedUsers().get(0).isBot())) {
            //Il veux les infos sur qqun
            User cible = message.getMentionedUsers().get(0);
            displayInfos(guild, cible, guild.getMember(cible), (TextChannel) channel);
            return;
        }

        displayInfos(guild, user, message.getMember(), (TextChannel) channel);
    }

    private void displayInfos(Guild guild, User user, Member member, TextChannel tc) {
        PlayerDataRegistery pdr = PlayerDataRegistery.getInstance();
        int xp = pdr.getXp(user.getIdLong());
        int messages = pdr.getMessages(user.getIdLong());
        long rolesAmount = Functions.countRoles(member.getRoles());
        String rolesList = Functions.displayRoles(member.getRoles());

        EmbedBuilder me = new EmbedBuilder()
                .setAuthor(user.getName(), null, user.getAvatarUrl())
                .setColor(Color.lightGray)
                .addField("Nom d'usage: ", member.getEffectiveName(), false)
                .addField("Première connexion: ", formatShort(member.getJoinDate()), false)
                .addField(
                        rolesAmount > 1 ? "Roles (" + rolesAmount + "):" : "Role (" + rolesAmount + "):",
                        rolesAmount == 0 ? "Cet utilisateur ne possède aucun rôle." : rolesList,
                        false
                )
                .addField(
                		"Power total: ", 
                		String.format("%s (%s/%s)", commandMap.getTotalUserPower(guild, user), commandMap.getPersonalUserPower(guild, user), commandMap.getRankUserPower(guild, user)), 
                		false
                );

        if (commandMap.isSuperAdmin(user)) {
            me.addField("Permissions supplémentaires:", "Vous êtes super-administrateur.", false);
        }

        me.addField("Messages envoyés: ", Integer.toString(messages), false)
                .addField("Experience: ", Integer.toString(xp), false);

        tc.sendMessage(me.build()).queue();
    }
}