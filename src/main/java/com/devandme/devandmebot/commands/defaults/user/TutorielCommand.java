/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.commands.defaults.user;

import static com.devandme.devandmebot.utils.Functions.sendPrivateMessage;

import java.awt.Color;
import java.util.HashMap;

import com.devandme.devandmebot.commands.Command;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent;

public class TutorielCommand {
	
	private static HashMap<Long, Integer> tutorielpage = new HashMap<>();
	
	protected final static int min = 0;
	protected final static int max = 6;
	
	@Command(name="tutoriel", description="Démmarer le tutoriel !")
	private void tutos(Guild guild, User user, MessageChannel channel, Message message, String[] args){
		Message mes = channel.sendMessage(loadPage(min)).complete();
		tutorielpage.put(mes.getIdLong(), min);
		/*
		 * React : 
		 * http://unicode.org/emoji/charts/full-emoji-list.html
		 * https://apps.timwhitlock.info/emoji/tables/unicode
		 * (Prendre les natifs)
		 */
		mes.addReaction("⏪").queue();
		mes.addReaction("⏩").queue();
		mes.addReaction("❌").queue();
		
		sendPrivateMessage(user, "[Dev&Me] Les règles de bases sont les suivantes : bonne ambiance, respect des autres, entre-aide ! Il est également important de noté que le recrutement est interdit sur tout le Discord.");
	}

	public static void onReact(MessageReactionAddEvent event) {
		if((tutorielpage.containsKey(event.getMessageIdLong())) && (!event.getUser().isBot())){
			String reactEmot = event.getReactionEmote().getName().toString();
			
			event.getTextChannel().clearReactionsById(event.getMessageIdLong()).complete();
			event.getTextChannel().addReactionById(event.getMessageIdLong(), "⏪").queue();
			event.getTextChannel().addReactionById(event.getMessageIdLong(), "⏩").queue();
			event.getTextChannel().addReactionById(event.getMessageIdLong(), "❌").queue();
			
			if((reactEmot.equalsIgnoreCase("⏪")) || (reactEmot.equalsIgnoreCase("⏩"))){
				int pageNumber = tutorielpage.get(event.getMessageIdLong());
				event.getTextChannel().editMessageById(event.getMessageId(), (reactEmot.equalsIgnoreCase("⏪") ? loadPage(((pageNumber-1) < min) ? min : pageNumber-1) : loadPage(((pageNumber+1) > max) ? max : pageNumber+1))).queue();
				tutorielpage.remove(event.getMessageIdLong());
				tutorielpage.put(event.getMessageIdLong(), (reactEmot.equalsIgnoreCase("⏪") ? pageNumber-1 : pageNumber+1));
			}else if(reactEmot.equalsIgnoreCase("❌")){
				event.getTextChannel().deleteMessageById(event.getMessageIdLong()).queue();
				tutorielpage.remove(event.getMessageIdLong());
			}
		}
	}
	
	private static MessageEmbed loadPage(int pageId){	
		String title = "_Aucune titre fourni !_";
		String content = "_Erreur lors du chargement !_";
		switch (pageId) {
		case 0: 
			title = "Page 0 - Bienvenue dans ce tutoriel:";
			content = "Bienvenue dans ce tutoriel qui a pour but de tout vous expliquer sur le Discord 'Dev & Me'.\n\nTout d'abord, une explication rapide du fonctionnement de ce tutoriel. Vous pouvez utilisez les flèches directionnels pour passer à la page suivante ou revenir à la précédante. Une fois que vous aurez terminé, appuyer sur la croix rouge pour mettre fin au tutoriel. Vous pourrez ensuite y revenir à tout moment grâce à la commande **!tutoriel**.";
			break;
		case 1:
			title = "Page 1 - Les salons:";
			content ="Le Discord 'Dev & Me' est principalement destiné aux développeurs ou aux personnes intéresser par l'univers de la programmation. Ainsi, vous pouvez demander de l'aide _(nous vérons dans la suite comment)_, discuter de la programmation ou de l'informatique en général. Vous ne serrez pas limité par vos compétences sur le Discord, que vous ne maitrisiez aucun langages ou plusieurs dizaines, aucune importance, tout le monde est le bienvenue sur le Discord ! De nombreux salons sont à votre disposition que ce soit pour parler de sujets sérieux, se divertir, posser des questions sur la programation, partages des sites/etc... ou simplement vous divertir. Le rôle de chaque salon est expliqué dans sa decription. Les informations importantes et/ou drôles spécifique à un salon sont quant à eux épinglé.";
			break;
		case 2:
			title = "Page 2 - Présentation & Communauté:";
			content = "Un des atouts du Discord est sûrement ses membres actif et sympatiques. Nous sommes devenus un vrai groupe soudé de passioner autour de la programtion ! Pour poursuivre cette bonne ambiance le plus longtemps possible, nous vons invitons à vous présenter rapidement dans #presentation. Vous pouez vous inspirez des présentations déjà faîtes ou faire une présentation totalement innvoante !\n_Nota: Le recrutement est interdit et passible de sanction._";
			break;
		case 3:
			title = "Page 3 - Les demandes d'aide:";
			content = "Le Discord se différencie également par son système d'aide unique. En effet, dès que vous souhaitez demander de l'aide, rendez vous dans le salon #bot-et-spam et entrez la commande **!problem create (langage1,langage2) (titre...) **. Cette commande va vous créé un salon spécifique **seulement** destiné à trouver une solution rapide à votre problème ! Les autres membres du Discord viendront donc vous aider jusqu'a que votre problème soit résolu. Une fois que votre problème à trouver une solution, utilsiez la commande **!problem solved (id)** pour fermé votre problème. Il passera donc en Résolu. Vous pourrez le réouvrir à tout moment _(!problem unsolved (id))_ ou le vissionez via _!problem view (id)_. Pour voir toutes les commandes relatives aux problèmes, tappez **!problem**.";
			break;
		case 4:
			title = "Page 4 - Les grades:";
			content = "Il existe sur le Discord trois types de grades:\n    - Les grades pour l'équipe du Discord (appelé _Personnel_ à savoir : adminsitrateur, modérateur et support)\n    - Les grades d'amis (les amis et les anciens membres du personnel)\n    - Les grades corresponant aux langages\nLes grades des deux premières catégories ne s'obtienent que en étant directement donné par le personnel sous certaines conditions, il n'est donc pas possible de les demander. Pour les grades correspondant aux langages (3ième catégorie) ils seront donné en fonction de vos aptitudes. Lorsqu'un membre du personnel et/ou le bot voit que vous être actif pour aider les personnes (et que vous avez manifestement des connaisances), vous pourrez obtenir le grade correspondant au langage. Si vous faites une présentation dans le salon adapté, vous obtiendrait le grade '─● Développeur ●'. Sachez qu'un grade de 3ième catégorie ne vous apporte pas d'avantages direct sur le Discord, il vous remercie simplement pour votre participation.";
			break;
		case 5:
			title = "Page 5 - Le personnel:";
			content = "L'équipe du Discord se compose actuellement de cinq personnes _(or les Bots)_, trois administrateurs et deux modérateurs.\n\nAdministrateurs: Arthur340, Théo | OriginalPainZ, Mathéo/DiscowZombie.\nModérateurs: Myuto, CanardOWhisky\n\nAnciens membres de l'équipe: Retr0 [Fondateur], Alexis (6ela/Sixela) [Modérateur]\n\n_Date de création du Discord: 17 juillet 2017_.\nPassage en version.2: 26 décembre 2017.";
			break;
		case 6:
			title = "Page 6 - Les Bots:";
			content = "Les Bots, littéralement robots ne sont pas des personnes physiques. Il apparaisant dans les membres du personnels mais ne sont en réalité que des applications programmé. Ils ont pour but de nous aider dans la gestion quotidienne du discord (gestions des problèmes), sanctionner les comportements déplacé ou vous divertir (jouer de la musique).";
			break;
		default:
			break;
		}
		EmbedBuilder em = new EmbedBuilder();
		em.setColor(Color.orange);
		em.setTitle(title);
		em.addField("", content, false);
		return em.build();
	}

}
