/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.commands.defaults.user;

import java.util.ArrayList;
import java.util.List;

import com.devandme.devandmebot.commands.Command;
import com.devandme.devandmebot.commands.Command.ExecutorType;
import com.devandme.devandmebot.utils.Functions;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.User;

public class RolesCommand {
	
	@Command(name="role", type=ExecutorType.ALL, description="Affiche la liste des rôles")
	private void problem(Guild guild, User user, MessageChannel channel, Message message, String[] args){
		List<Role> langages = collectLangagesRoles(guild.getRoles());
		channel.sendMessage("Les langages suivants sont disponibles sur le Discord: \n"+Functions.displayRoles(langages)).queue();
	}
	
	private List<Role> collectLangagesRoles(List<Role> allroles){
		List<Role> langages = new ArrayList<>();
		boolean collect = false;
		for(Role r : allroles){
			if(collect && r.getName().equalsIgnoreCase("──── Everyone ────")) collect = false;
			if(collect) langages.add(r);
			if(!collect && r.getName().equalsIgnoreCase("─● Développeur ●")) collect = true;
		}
		return langages;
	}

}
