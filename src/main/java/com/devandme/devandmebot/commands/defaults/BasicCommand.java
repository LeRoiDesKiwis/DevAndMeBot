/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.commands.defaults;

import java.awt.Color;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Branch;
import org.gitlab4j.api.models.Commit;
import org.gitlab4j.api.utils.ISO8601;

import com.devandme.devandmebot.commands.Command;
import com.devandme.devandmebot.commands.Command.ExecutorType;
import com.devandme.devandmebot.commands.CommandMap;
import com.devandme.devandmebot.commands.SimpleCommand;
import com.devandme.devandmebot.core.DevAndMe;
import com.devandme.devandmebot.utils.Constantes;
import com.devandme.devandmebot.utils.MessageManager.MessagesList;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

import static com.devandme.devandmebot.utils.MessageManager.*;

public class BasicCommand {
	
	private final DevAndMe dam;
	private final CommandMap commandMap;
	private final String gitlabtoken;
	
	public BasicCommand(DevAndMe dam, CommandMap commandMap, String gitlabtoken){
		this.dam = dam;
		this.commandMap = commandMap;
		this.gitlabtoken = gitlabtoken;
	}
	
	@Command(name="stop", type=ExecutorType.CONSOLE_OR_S_USER, description="Eteindre le bot")
	private void stop(){
		dam.stop();
	}
	
	@Command(name="help", type=ExecutorType.ALL_USER, description="Affiche le message d'aide")
	private void help(Guild guild, User user, MessageChannel channel){
	    
	    EmbedBuilder help = new EmbedBuilder();
	    help.setTitle(getMessage(MessagesList.CMD_HELP_LIST));
	    help.setColor(Color.orange);
	    
	    for(SimpleCommand cmd : commandMap.getCommands()){
			if(cmd.getType() == ExecutorType.CONSOLE || cmd.getType() == ExecutorType.S_USER || cmd.getType() == ExecutorType.CONSOLE_OR_S_USER) continue;
			if((guild == null) || (cmd.getPower() > commandMap.getTotalUserPower(guild, user))) continue;
			//Permet de masquer de l'affichage les commandes ayant un power de "-1"
			if(cmd.getPower() == -1) continue;

			if(cmd.getPower() > 0) help.addField(cmd.getName() + " (Power: " + Integer.toString(cmd.getPower()) + ")", cmd.getDescription(), false);
			else help.addField(cmd.getName() + (cmd.getName().equalsIgnoreCase("quiz") ? " _(Bientôt disponible !)_" : ""), cmd.getDescription(), false);
		}
	    
	    channel.sendMessage(help.build()).queue();
	}
	
	private Long lastcheck = 0L;
	private HashMap<Branch, Commit> lastcommits = new HashMap<>();
	
	@Command(name="botinfo", type=ExecutorType.ALL_USER, description="Affiche les infos du bot")
	private void botinfo(Guild guild, MessageChannel channel){
		String message = "Latence: "+guild.getJDA().getPing()+"ms\nVersion du bot: "+Constantes.getVersion();
		GitLabApi gitLabApi = new GitLabApi("https://gitlab.com", gitlabtoken);
		
		try {
			Date since = ISO8601.toDate("2017-01-01T00:00:00Z");
			Date until = new Date();
			DateFormat dateFormat = new SimpleDateFormat("E dd MMMMM yyyy - HH:mm");
			
			message = message + "\nDernière mise à jour:";
			
			//Si la dèrnière mise à jour remonte à plus de 15 minutes ou que la liste est vide
			if((System.currentTimeMillis() > lastcheck + 900000L) || (lastcommits.size() == 0)){
				//4441153 -> Id du projet "Dev & Me Bot"
				for(Branch b : gitLabApi.getRepositoryApi().getBranches(4441153)){
					Commit last = gitLabApi.getCommitsApi().getCommits(4441153, b.getName(), since, until).get((Integer)0);
					if(lastcommits.containsKey(b)) lastcommits.remove(b);
					lastcommits.put(b, last);
				}
				lastcheck = (System.currentTimeMillis());
			}
			
			for(Entry<Branch, Commit> e : lastcommits.entrySet()){
				message = message + "\n    Branche: "+e.getKey().getName()+"\n          Auteur: "+e.getValue().getCommitterName()+"\n          Date: "+dateFormat.format(e.getValue().getCommittedDate())+"\n          Title: "+e.getValue().getTitle();
			}
			
		} catch (ParseException | GitLabApiException e1) {
			e1.printStackTrace();
		}
		
		message = message + "\nParticipe à mon développement sur https://gitlab.com/devandme/DevAndMeBot.";
		channel.sendMessage(message).queue();
	}

}