/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.commands.defaults.admin;

import java.util.HashMap;

import com.devandme.devandmebot.commands.Command;
import com.devandme.devandmebot.commands.Command.ExecutorType;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

public class MassMessage {
	
	/*
	 * Ne fonctionne pas, surement du à une erreur dans JDA qui n'envoye pas correctement les MP.
	 */
	
	public static HashMap<Long, String> waiting = new HashMap<>();
	
	@Command(name="massmessage", type=ExecutorType.ALL_USER, description="Permet d'envoyer des messages à tous les membres", power=90)
	private void massmessage(Guild guild, User user, MessageChannel channel, Message message, String[] args){
		channel.sendMessage("Bientot disponible !").queue();
		/*
		if(args.length == 0){
			channel.sendMessage("!massmessage (message)").queue();
			return;
		}
		
		boolean forceSend = false;
		StringBuilder sb = new StringBuilder();
		for(int j = 0; j < args.length; j++){
			if(args[j].equalsIgnoreCase("--yes")){
				forceSend = true;
			}
			sb.append(args[j]).append(" ");
		}
		String msg = sb.toString().trim();
		
		if(forceSend){
			//force send
			send(guild, msg.replaceFirst(" --yes", ""));
			return;
		}

		channel.sendMessage("Vous êtes sur le point d'envoyer ce message: ''"+msg+"'' à **"+guild.getMembers().size()+"** membres. Pour valider, écrivez **yes** sinon écrivez autre chose.").queue();
		waiting.put(user.getIdLong(), msg);*/
	}
	
	public static void send(Guild guild, String message){
		for(Member m : guild.getMembers()){
			User u = m.getUser();
			if(!u.hasPrivateChannel()){
				try{
					u.openPrivateChannel().queue();
				}catch(Exception e){}
			}
			try{
				((Message) u).getPrivateChannel().sendMessage(message).queue();
			}catch(Exception e){}
		}
	}

}
